	<footer>
		<div class="wrapper">

			<div id="hrg">
				<a href="http://www.heavyrestaurantgroup.com/" rel="external">Heavy Restaurant Group</a>					
			</div>


			<div id="social">
				<div class="links">
					<a href="<?php the_field('facebook'); ?>" rel="external">
						<img src="<?php bloginfo('template_directory') ?>/images/facebook.svg" alt="Facebook" />
					</a>

					<a href="<?php the_field('instagram'); ?>" rel="external">
						<img src="<?php bloginfo('template_directory') ?>/images/instagram.svg" alt="Instagram" />
					</a>

					<a href="<?php the_field('twitter'); ?>" rel="external">
						<img src="<?php bloginfo('template_directory') ?>/images/twitter.svg" alt="Twitter" />
					</a>
				</div>					
			</div>

		</div>
	</footer>

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>