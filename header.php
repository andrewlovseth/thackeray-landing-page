<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<link rel="stylesheet" href="https://use.typekit.net/mde8cyz.css">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	
	<?php wp_head(); ?>

</head>
<body class="home cover" style="background-image: url(<?php $backgroundImage = get_field('background_image'); echo $backgroundImage['url']; ?>);">

	<?php if(get_field('show_announcement_bar')): ?>
		
		<section id="announcement">
			<div class="wrapper">
				
				<?php the_field('announcement'); ?>

			</div>
		</section>


	<?php endif; ?>

	<header>
		<div class="wrapper">

			<section class="logo">
				<img src="<?php $logo = get_field('logo'); echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />

			</section>

		</div>
	</header>