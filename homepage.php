<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="info">
		<div class="wrapper">

			<section id="top">

				<div id="hours">
					<h3>Hours</h3>
					<?php the_field('hours'); ?>
				</div>

				<div id="location">
					<h3>Location</h3>
					<?php the_field('location'); ?>
				</div>

				<div id="menu">
					<h3>Menu</h3>

					<?php if(have_rows('menus')): ?>
						<div class="menu-wrapper">
							<?php while(have_rows('menus')): the_row(); ?>
							 
								<div class="cta">
									<a href="<?php the_sub_field('pdf'); ?>" class="btn" rel="external"><?php the_sub_field('label'); ?></a>
								</div>

							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>

			</section>
		
			<section id="bottom">

				<div class="col reservations">
					<?php the_field('reservations'); ?>
				</div>

				<div class="col gift-cards">
			    	<div class="cta">
				    	<a href="<?php the_field('gift_cards_link'); ?>" class="btn" rel="external">Gift Cards</a>
				    </div>
				</div>

				<div class="col order">					

			    	<div class="cta ctas">
				    	<a href="<?php the_field('delivery_link'); ?>" class="btn" rel="external">Delivery</a>
				    	<a href="<?php the_field('pick_up_link'); ?>" class="btn" rel="external">Pick-Up</a>
				    </div>
				</div>

			</section>	


		</div>
	</section>


<?php get_footer(); ?>